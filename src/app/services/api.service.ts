import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getMovies() {
    return this.http.get('https://swapi.co/api/films')
  }

  addMovie(title: string, director: string, release: string) {
    return this.http.post('https://httpbin.org/post', {
      title, director, release,
    })
  }

}
