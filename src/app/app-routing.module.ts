import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { MovieListComponent } from './components/movie-list/movie-list.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },

  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: 'movies', component: MovieListComponent, canActivate: [ LoggedInGuard ] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  providers: [ LoggedInGuard ],
})
export class AppRoutingModule { }
