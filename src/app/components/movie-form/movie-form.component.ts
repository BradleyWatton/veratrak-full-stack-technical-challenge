import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-movie-form',
  templateUrl: './movie-form.component.html',
  styles: [],
})
export class MovieFormComponent implements OnInit {

  public visible: boolean = false;

  private title: string = '';
  private director: string = '';
  private release: string = '';

  private response: any;

  constructor(private api: ApiService) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if(!form.valid) return

    console.log(form);
    console.log(this.api);
    this.api.addMovie(this.title, this.director, this.release).subscribe((data: any) => {
      this.response = data;
    })
  }

  public open() {
    this.visible = true;
  }

  public close() {
    this.visible = false;
  }

}
