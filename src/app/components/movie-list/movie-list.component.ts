import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { MovieFormComponent } from '../movie-form/movie-form.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  @ViewChild('movieForm') movieForm: MovieFormComponent;

  private loading: boolean = false;
  private movies: any[] = [];

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.api.getMovies().subscribe((data: any) => {
      this.movies = data.results.sort(this.movieSort);
      this.loading = false;
    })
  }

  private movieSort(a: any, b: any) {
    if(a.episode_id > b.episode_id) return 1
    if(a.episode_id < b.episode_id) return -1

    return 0
  }

  private openMovieForm() {
    this.movieForm.open();
  }

  private logout() {
    localStorage.removeItem('user');
    this.router.navigate([ '/login' ]);
  }

}
