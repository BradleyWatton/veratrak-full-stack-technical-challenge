import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    if(!!localStorage.getItem('user')) this.router.navigate([ '/' ])
  }

  onSubmit(form: NgForm) {
    // if(!form.valid) return

    localStorage.setItem('user', 'true')

    this.router.navigate([ '/' ])
  }

}
