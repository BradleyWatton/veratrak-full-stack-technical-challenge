# Veratrak full-stack technical challenge
Here's my submission for the Veratrak full-stack technical challenge. The design is a bit rough around the edges in order to speed things up but I believe I have completed everything to the specification. Running is as simple as `yarn`/`npm i` then `yarn start`/`npm start`.

### 3. State management
To share data between components, you would use `Inputs` (I know them as `Props` from using Vue.js). Example of an `Input` definition:

```typescript
// Child
class MovieView {

  @Input() movieId!: string;

}
```

Example of how you'd use `Input`:
```html
<!-- Parent -->
<app-movie-view [movieId]="123"></app-movie-view>
```

You could also use a `Service` for managing data. Definition:

```typescript
@Injectable({
  providedIn: 'root'
})
export class DataService {

  selectedMovie: Movie;

}
```

Usage:
```typescript
@Component(...)
export class MovieViewComponent implements OnInit {

  constructor(private data: DataService) { } // dependency injection

}
```

```html
<!-- MovieViewComponent -->
<p>
  {{ data.selectedMovie.title }}
</p>
```